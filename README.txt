d3_tagcloud

This module allows you to create a block with terms from a taxonomy. Allows to select which taxonomy to render and select which terms to show. Terms can also be shown in different sizes depending on their importance, configurable from a taxonomy field.