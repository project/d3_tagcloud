<?php

/**
 * Act on tag cloud data before it is rendered.
 */
function hook_d3_tagcloud_render_alter(&$render) {
  // Change the importance of the term with tid 20.
  $render[20]['importance'] = 100;
}
